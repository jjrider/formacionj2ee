package es.wul4.formacion.spring.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import es.wul4.formacion.spring.service.MensajeService;

public class Main {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
			ApplicationContext context = new 	ClassPathXmlApplicationContext(
				"es/wul4/formacion/spring/test/spring-test.xml");
			MensajeService hello = (MensajeService) context.getBean("MensajeService");
			System.out.println(hello.sayMessage(args[0]));

	}
}
