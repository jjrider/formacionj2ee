package es.wul4.formacion.spring.service;

public interface MensajeService {

	public abstract String sayMessage(String name);

}