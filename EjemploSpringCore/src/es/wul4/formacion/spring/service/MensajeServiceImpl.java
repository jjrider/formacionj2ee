package es.wul4.formacion.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.wul4.formacion.spring.model.Mensaje;

@Service(value="MensajeService")
public class MensajeServiceImpl implements  MensajeService {
	
	@Autowired
	private Mensaje msg = null;

	public void setMsg(Mensaje msg) {
		this.msg = msg;
	}
	

	/* (non-Javadoc)
	 * @see es.wul4.formacion.spring.service.MensajeService#sayMessage(java.lang.String)
	 */
	@Override
	public String sayMessage(String name) {
		return msg.getMsg()+ " " + name;
	}

}
