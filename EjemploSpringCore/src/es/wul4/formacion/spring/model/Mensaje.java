package es.wul4.formacion.spring.model;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository(value="MensajeBienvenida")
public class Mensaje {
	
	@Value("${mensaje.mes}")
	private String msg=null;
	
	
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	
	
}
